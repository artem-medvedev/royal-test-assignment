var gulp = require('gulp'),
    path = require('path'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    csso = require('gulp-csso'),
    uglify = require('gulp-uglify'),
    cmq = require('gulp-combine-media-queries'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    fileinclude = require('gulp-file-include');

var bc = './bower_components/';

// HTML Task
gulp.task('html', function() {
    gulp.src(['dev/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
        }))
        .pipe(gulp.dest('./build'))
        .pipe(browserSync.stream());
});

// Sass task
gulp.task('sass', function () {
    gulp.src('dev/scss/**/*.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(concat('style.min.css'))
        .pipe(cmq()) // Combine media queries
        .pipe(csso()) // Minify CSS
        .pipe(gulp.dest('build/css/'))
        .pipe(browserSync.stream());
});

// Scripts Task
gulp.task('scripts', function() {
    gulp.src([
            'dev/scripts/main.js'
        ])
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('build/scripts'))
        .pipe(browserSync.stream());
});

// Libs Task
gulp.task('libs', function() {
    gulp.src(bc+'jquery/dist/jquery.min.js')
        .pipe(gulp.dest('build/libs/jquery/'));
});

// Watch Task
gulp.task('watch', function() {
    gulp.watch('dev/**/*.html', ['html']);
    gulp.watch('dev/scss/**/*.scss', ['sass']);
    gulp.watch('dev/**/*.js', ['scripts']);
});

// Browsersync Task
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./build/"
        },
        browser: 'Google Chrome' // OS X : 'Google Chrome', Windows : 'chrome'
    });
});

// Default task
gulp.task('default', [
    'html',
    'sass',
    'scripts',
    'libs',
    'browser-sync',
    'watch'
]);