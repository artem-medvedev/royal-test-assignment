$(document).ready(function() {
	// Highlight checked row
	function highlightRow(checkbox, row) {
		if ( $(checkbox).is(':checked') ) {
			row.addClass('checked');
		} else {
			row.removeClass('checked');
		}
	}

	// Check all checkboxes
	$("#checkAll").change(function () {
		var $checkboxes = $(".checkbox__input:not(.notes__checkbox input)"),
			$row = $('.table__item--head');

	    $checkboxes.prop('checked', $(this).prop("checked"));
	    highlightRow($(this), $row);
	});

	$('.notes-check-all').change(function() {
		$(this).closest('.notes__table').find('.checkbox__input').prop('checked', $(this).prop("checked"));
	});
	
	// Toggle active class on table row when checkbox is checked
	$('.checkbox__input').change(function() {
		var $row = $(this).closest('.table__item--head');
		highlightRow($(this), $row);
	});

	$('.checkbox__input, .checkbox__checkmark, .request__link').click(function(e) {
		e.stopPropagation();
	});

	// Show/hide request body
	$('.table__item--head').click(function() {
		$(this).closest('.table__item').toggleClass('collapsed');
		$(this).siblings('.table__item--body').slideToggle();
	});
});